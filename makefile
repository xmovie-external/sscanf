#
#  sscanf 2.13.2
#
#  Version: MPL 1.1
#
#  The contents of this file are subject to the Mozilla Public License Version
#  1.1 (the "License"); you may not use this file except in compliance with
#  the License. You may obtain a copy of the License at
#  http://www.mozilla.org/MPL/
#
#  Software distributed under the License is distributed on an "AS IS" basis,
#  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
#  for the specific language governing rights and limitations under the
#  License.
#
#  The Original Code is the sscanf 2.0 SA:MP plugin.
#
#  The Initial Developer of the Original Code is Alex "Y_Less" Cole.
#  Portions created by the Initial Developer are Copyright (c) 2022
#  the Initial Developer. All Rights Reserved.
#
#  Contributor(s):
#
#      Cheaterman
#      Emmet_
#      karimcambridge
#      kalacsparty
#      Kirima
#      leHeix
#      maddinat0r
#      Southclaws
#      Y_Less
#      ziggi
#
#  Special Thanks to:
#
#      SA:MP Team past, present, and future.
#      maddinat0r, for hosting the repo for a very long time.
#      Emmet_, for his efforts in maintaining it for almost a year.
#

# This file demonstrates how to compile the sscanf project on Linux.
#
# To compile SSCANF do:
#
# make SSCANF
#

GPP = g++
GCC = gcc
SSCANF_OUTFILE = "sscanf.so"

COMPILE_FLAGS = -m32 -fPIC -c -O2 -w -D LINUX -D PROJECT_NAME=\"sscanf\" -D HAVE_STDINT_H -D GLM_FORCE_SSE2 -D GLM_FORCE_QUAT_DATA_WXYZ -D NOMINMAX -D nssv_CONFIG_SELECT_STRING_VIEW=nssv_STRING_VIEW_NONSTD -D span_CONFIG_SELECT_SPAN=span_SPAN_NONSTD -I ./SDK/amx/ -I ./robin-hood-hashing/src/include -I ./string-view-lite/include -I ./open.mp/include -I ./span-lite/include -I ./glm

SSCANF = -D SSCANF $(COMPILE_FLAGS)

all: SSCANF

clean:
	-rm -f *~ *.o *.so

SSCANF: clean
	$(GPP) $(SSCANF) ./SDK/*.c
	$(GPP) $(SSCANF) *.cpp
	$(GPP) -m32 -fPIC -shared -o $(SSCANF_OUTFILE) *.o

